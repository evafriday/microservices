# Microservices

Our tool provides a simple website for addition and subtraction service.
Users are asked to enter two numbers.
Then, the result is provided on the next page as json.

## Basic Architecture
Each interactive mathematical service has its own view that connects to the mathematical operation that it calls upon.

## How-to Run the Application
1. Clone repository
2. Check if all projects are added as maven projects. If not, do so.
3. Start each service in service repository, following this order:
        a. Config Server (Port 8888)
        b. Eureka Server (Port 1111)
        c. Addition Server (Port 2222)/ Subtraction Server (Port 3333)
        d. Subtraction Server/ Addition Server

4. You can also build docker images in each service repository, and then run the docker container of this image.