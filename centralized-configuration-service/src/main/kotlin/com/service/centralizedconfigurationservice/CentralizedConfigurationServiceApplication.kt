package com.service.centralizedconfigurationservice

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.config.server.EnableConfigServer


@EnableConfigServer
@SpringBootApplication
class CentralizedConfigurationServiceApplication

fun main(args: Array<String>) {
	runApplication<CentralizedConfigurationServiceApplication>(*args)
}
