package com.service.subtractionserver

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
@RestController
class SubtractionController {
  //  protected var logger: Logger = Logger.getLogger(SubtractionController::class.java
  //          .getName())

    @RequestMapping("/subtract")
    fun doSubtract(@RequestParam(defaultValue = "0") minuend: String,
                   @RequestParam(defaultValue = "0") subtrahend: String): String {
        val m: Int = Integer.valueOf(minuend)
        val s: Int = Integer.valueOf(subtrahend)
        val difference = m - s
        return "{\"minuend\":\"$minuend\", \"subtrahend\":\"$subtrahend\", \"difference\": \"$difference\"}"
    }
}