package com.service.subtractionserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

//these things must be enabled once configserver and eureka are set
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.cloud.client.discovery.EnableDiscoveryClient

//@EnableAutoConfiguration
@EnableDiscoveryClient
@SpringBootApplication
class Subtractionserver

		fun main(args: Array<String>) {
			runApplication<Subtractionserver>(*args)
		}