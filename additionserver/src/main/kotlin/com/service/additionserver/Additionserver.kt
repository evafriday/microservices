package com.service.additionserver

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

//these things must be enabled once configserver and eureka are set
//import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.context.config.annotation.RefreshScope

//for updated values from config-server:
@RefreshScope
@EnableDiscoveryClient
@SpringBootApplication
class Additionserver
		fun main(args: Array<String>) {
			runApplication<Additionserver>(*args)
		}
