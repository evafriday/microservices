package com.service.additionserver

import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController


@RestController
class AdditionController {
    @RequestMapping("/add")
    fun doAdd(@RequestParam(defaultValue = "0") addend1: String,
              @RequestParam(defaultValue = "0") addend2: String): String {
        val addend1 = Integer.valueOf(addend1)
        val addend2 = Integer.valueOf(addend2)
        val sum = addend1 + addend2
        return "{\"addend1\":\"$addend1\", \"addend2\":\"$addend2\", \"sum\": \"$sum\"}"
    }
}